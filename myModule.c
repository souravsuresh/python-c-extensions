#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
 

static PyObject* name(PyObject* self, PyObject* args)
{
    printf("My Name is Sourav Suresh\n");
    return Py_None;
}
static PyObject* play(PyObject* self, PyObject* args)
{
    int inp_no;
    int comp;
    char *val[3];
    val[0] = "Stone";
    val[1] = "Paper";
    val[2] = "Scissor";
    printf("\n****** Lets Play Stone Paper Scissors *******\n");
    printf("Instructions: \n \
        1. Enter 0 for Stone\n \
        2. Enter 1 for Paper\n \
        3. Enter 2 for Scissor\n");

    // int isFin = 0;
    for(;;)
    {
        comp = rand() % 3;
        scanf("%d",&inp_no);
        printf("Computer: %s\tPlayer: %s\n",val[comp],val[inp_no]);
        if(comp == inp_no)
        {
            printf("----- Awesome ------\n");
        }
        else if(comp == 0 && inp_no == 1)
        {
            return Py_BuildValue("i", 1);
        }
        else if(comp == 0 && inp_no == 2)
        {
            return Py_BuildValue("i", 0);
        }
        else if(comp == 1 && inp_no == 0)
        {
            return Py_BuildValue("i", 0);
        }
        else if(comp == 1 && inp_no == 2)
        {
            return Py_BuildValue("i", 1);
        }
        else if(comp == 2 && inp_no == 0)
        {
            return Py_BuildValue("i", 1);
        }
        else if(comp == 2 && inp_no == 1)
        {
            return Py_BuildValue("i", 0);
        }
        else
            printf("Please Enter valid number");
    }
    return Py_None;
}

static PyMethodDef sosMethods[] = {
    {"name", name, METH_NOARGS, "Basic Function"},
    {"play", play, METH_NOARGS, "Play Function"},
    {NULL, NULL, 0, NULL }
};

static struct PyModuleDef myModule = {
    PyModuleDef_HEAD_INIT,
    "sos",
    "My Module",
    -1,
    sosMethods
};

PyMODINIT_FUNC PyInit_sos(void)
{
    return PyModule_Create(&myModule);
}
// #include <Python.h>

// // Function 1: A simple 'hello world' function
// static PyObject* helloworld(PyObject* self, PyObject* args)
// {
//     printf("Hello World\n");
//     return Py_None;
// }

// // Our Module's Function Definition struct
// // We require this `NULL` to signal the end of our method
// // definition
// static PyMethodDef myMethods[] = {
//     { "helloworld", helloworld, METH_NOARGS, "Prints Hello World" },
//     { NULL, NULL, 0, NULL }
// };

// // Our Module Definition struct
// static struct PyModuleDef myModule = {
//     PyModuleDef_HEAD_INIT,
//     "myModule",
//     "Test Module",
//     -1,
//     myMethods
// };

// // Initializes our module using our above struct
// PyMODINIT_FUNC PyInit_myModule(void)
// {
//     return PyModule_Create(&myModule);
// }
